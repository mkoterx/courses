Books
https://drive.google.com/open?id=16NOICl543X8-TX6C-FB5HfIvVyqTou4f

Links
Installation guide https://wiki.archlinux.org/index.php/Installation_guide  
Core utilities https://wiki.archlinux.org/index.php/Core_utilities  
Nano https://wiki.archlinux.org/index.php/nano  
Vim https://wiki.archlinux.org/index.php/vim  
User and Group Administration https://wiki.archlinux.org/index.php/Users_and_groups  
Substitute User - su https://wiki.archlinux.org/index.php/Su  
Sudo https://wiki.archlinux.org/index.php/Sudo#Configuration  
Package Manager - Pacman https://wiki.archlinux.org/index.php/Pacman  
Pacman Tips https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks  
SSH - Secure Shell https://wiki.archlinux.org/index.php/OpenSSH  

Notes
-------------------
bootloader: 
-efi
-bios

toools for partitioning: fdisk, parted

# LINUX DIRECTORY STRUCTURE
/dev
    file desciptors for all devices reckognised

/media (before was /mnt)
    mount devices (device desc is in /dev)

/etc
    settings, confs, ...

/lost+found
    unnatached inodes bcause of some failure, uses the JFS (Journaling File System)

/opt
    additional stuff (software, ...)


pacman -S grub

/proc
    files that represent processes and system info

/run
    runtime info for application 
    ex: when app starts a socekt it writes its name here
    ex: when app is started at can write its PID here, so that afterwards (or other app) 
        can check  whether its running

/sbin
    system binary content

/tmp
    usually RAM file system (does not persist on disk)

/usr

/var

-------------------------------------------------------------------------
# best practice:
sepparate partitions: /home, /boot, swap  

on servers, should not have swap  

mounting atatch phusical partition in a folder   

installation:  
ls /sys/firmware/efi/efivars // check if UEFI or BOOT  
ping 8.8.8.8 // check internet connection  
timedatectl set-ntp true  
lsblk // list block devices (alternative: fdisk -l)  
fdisk /dev/sda // start fdisk against your drive as root  
o  
n  
p  
w  
mkfs.ext4 /dev/sda1 // format   
mkfs.ext4 /dev/sda2 // format  
mount /dev/sda1 /mnt  
cd /mnt  
mkdir home  
mount /dev/sda2 home  

pacstrap /mnt base // isntall the base packages  

genfstab -U /mnt >> /mnt/etc/fstab  

arch-chroot /mnt // change root into the new system (copies some state, processes...)  

pacman -S grub   

grub-install --target=i386-pc /dev/sda (sda - disk on which arch is installed, not partition like sda1 or sda2)  
grub-mkconfig -o /boot/grub/grub.cfg  

ln -sf /usr/share/zoneinfo/Europe/Skopje /etc/localtime // set the timezone  
hwclock --systohc  

Uncomment en_US.UTF-8 UTF-8 and other needed locales in /etc/locale.gen, and generate them with `locale-gen`  

Set the LANG variable in locale.conf (append the line: LANG=en_US.UTF-8 to the file)  

Network config.. edit /etc/hostname and /etc/hosts accordingly  

ip link show (ip a) // list network interfaces  
dhcpcd network_interface // (pr `dhcpcd enp0s3`) mozno e da ne treba (enp0s3 obicno e od virtual box so go dodluva.. ?) ova e za dodeluvanje na ip adresa, ne mora ako gi napravime slednite naredbi

systemctl enable dhcpdcd  
systemctl start dhcpdcd  

DNS:












